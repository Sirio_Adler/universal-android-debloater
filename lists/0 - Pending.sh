### These packages will be added in the default selection when I will find time to document them.
# It should be safe but you still need to be ***VERY** careful).

# REMINDER : "Safe" only means "does not cause bootloop". 
# These packages can definitely break features (but you can easily reinstall them with my script)

declare -a pending=(

	####### THIRD PARTY APPS ########


	############ SAMSUNG ############
	"com.samsung.android.unifiedprofile"
	"com.samsung.android.widgetapp.yagooedge.sport"
	"com.samsung.faceservice"
	"com.samsung.fresco.logging"
	"com.samsung.knox.securefolder.setuppage"
	"com.sec.android.widgetapp.webmanual"
	"com.sec.enterprise.knox.cloudmdm.smdms"


	############ SONY ############		
	"com.sonymobile.devicesecurity.service"
	"com.sonymobile.home.product.res.overlay"
	"com.sonymobile.indeviceintelligence"
	"com.sonymobile.swiqisystemservice"
	"com.sonymobile.themes.xperialoops2"
	"com.sonymobile.xperiaxlivewallpaper"
	"com.sonymobile.xperiaxlivewallpaper.product.res.overlay"

	)
